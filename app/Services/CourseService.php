<?php


namespace App\Services;


use App\Models\Course;
use App\Repositories\CourseRepository;
use Illuminate\Database\Eloquent\Model;

class CourseService
{
    public $courseRepository;

    public function __construct(CourseRepository $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }

    public function getAllCourses()
    {
        return $this->courseRepository->getAllCourses();
    }

    public function getActiveCourses($perPage)
    {
        return $this->courseRepository->getCourses($perPage);
    }

    public function createCourse($data)
    {
        $course = new Course();
        $course->fill($data);
        return $this->courseRepository->create($course);
    }

    public function courseDetails($id)
    {
        return $this->courseRepository->show($id);
    }

    public function updateCourse($id, $data)
    {
        return $this->courseRepository->edit($id, $data);
    }


    public function softDeleteCourse($id)
    {
        return $this->courseRepository->delete($id);
    }

    public function search($filters)
    {
        return $this->courseRepository->search($filters);
    }
}
