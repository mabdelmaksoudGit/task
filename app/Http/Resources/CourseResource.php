<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Log;

class CourseResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'rating' => $this->rating,
            'views' => $this->views,
            'hours' => $this->hours,
            'is_active' => $this->is_active,
            'level' => $this->levels,
            'category' => ($this->category) ? $this->category->name : '',
            'category_id' => $this->category_id ?? '',
        ];
    }
}
