<?php

namespace App\Http\Requests;

use App\Models\Course;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'description' => ['required', 'string'],
            'hours' => ['required', 'integer'],
            'is_active' => ['required', 'boolean'],
            'levels' => ['required', Rule::In(array_values(Course::courseLevels))],
            'category_id' => ['required', 'exists:categories,id'],
        ];
    }
}
