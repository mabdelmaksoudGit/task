<?php

namespace App\Http\Controllers\Mobile;

use App\Helpers\Facades\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Services\CategoryService;
use Illuminate\Http\Request;


class CategoryController extends Controller
{
    public $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function activeCategories(Request $request)
    {
        $perPage = $request->per_page ?? 5;
        $categories = $this->categoryService->getActiveCategory($perPage);
        return ApiResponse::success(200, CategoryResource::collection($categories), 'Success', $categories->toArray());
    }
}
