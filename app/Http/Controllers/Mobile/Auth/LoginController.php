<?php

namespace App\Http\Controllers\Mobile\Auth;

use App\Helpers\Facades\ApiResponse;
use App\Http\Controllers\Controller;


class LoginController extends Controller
{
    public function createToken()
    {
        $user = \App\Models\User::query()->first();
        $token = $user->createToken(config('app.name'))->plainTextToken;
        return ApiResponse::success(200, ['token' => $token], 'Success');
    }
}
