<?php

namespace App\Http\Controllers\Dashboard;

use App\Helpers\Facades\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\UpdateActivity;
use App\Http\Resources\CategoryResource;
use App\Services\CategoryService;


class CategoryController extends Controller
{
    public $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $categories = $this->categoryService->getAllCategories();
        return ApiResponse::success(200, CategoryResource::collection($categories), 'Success');
    }

    public function store(CategoryRequest $request)
    {
        $categories = $this->categoryService->createCategory($request->validated());
        return ApiResponse::success(200, new CategoryResource($categories), 'Success');
    }

    public function show($id)
    {
        $categories = $this->categoryService->categoryDetails($id);
        return ApiResponse::success(200, new CategoryResource($categories), 'Success');
    }

    public function update($id, CategoryRequest $request)
    {
        $categories = $this->categoryService->updateCategory($id, $request->validated());
        return ApiResponse::success(200, new CategoryResource($categories), 'Success');
    }

    public function destroy($id)
    {
        $this->categoryService->softDeleteCategory($id);
        return ApiResponse::success(200, [], 'Success');
    }

    public function updateCategoryActivity($id, UpdateActivity $request)
    {
        $categories = $this->categoryService->updateCategory($id, $request->validated());
        return ApiResponse::success(200, new CategoryResource($categories), 'Success');
    }

    public function activeCategories()
    {
        $categories = $this->categoryService->getActiveCategory();
        return ApiResponse::success(200, CategoryResource::collection($categories), 'Success');
    }

    public function search(SearchRequest $request)
    {
        $categories = $this->categoryService->search($request->validated());
        return ApiResponse::success(200, CategoryResource::collection($categories), 'Success');
    }
}
