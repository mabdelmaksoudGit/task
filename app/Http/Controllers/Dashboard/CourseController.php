<?php

namespace App\Http\Controllers\Dashboard;

use App\Helpers\Facades\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\CourseRequest;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\UpdateActivity;
use App\Http\Resources\CourseResource;
use App\Services\CourseService;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public $courseService;

    public function __construct(CourseService $courseService)
    {
        $this->courseService = $courseService;
    }

    public function index()
    {
        $courses = $this->courseService->getAllCourses();
        return ApiResponse::success(200, CourseResource::collection($courses), 'Success');
    }

    public function store(CourseRequest $request)
    {
        $courses = $this->courseService->createCourse($request->validated());
        return ApiResponse::success(200, new CourseResource($courses), 'Success');
    }

    public function show($id)
    {
        $courses = $this->courseService->courseDetails($id);
        return ApiResponse::success(200, new CourseResource($courses), 'Success');
    }

    public function update($id, CourseRequest $request)
    {
        $courses = $this->courseService->updateCourse($id, $request->validated());
        return ApiResponse::success(200, new CourseResource($courses), 'Success');
    }

    public function destroy($id)
    {
        $this->courseService->softDeleteCourse($id);
        return ApiResponse::success(200, [], 'Success');
    }

    public function updateCourseActivity($id, UpdateActivity $request)
    {
        $courses = $this->courseService->updateCourse($id, $request->validated());
        return ApiResponse::success(200, new CourseResource($courses), 'Success');
    }

    public function activeCourses()
    {
        $courses = $this->courseService->getActiveCourses();
        return ApiResponse::success(200, CourseResource::collection($courses), 'Success');
    }

    public function search(SearchRequest $request)
    {
        $courses = $this->courseService->search($request->validated());
        return ApiResponse::success(200, CourseResource::collection($courses), 'Success');
    }
}
