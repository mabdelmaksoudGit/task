<?php

namespace App\Repositories;

use App\Models\Category;
use App\Models\Course;
use App\Repositories\Interfaces\CategoryRepositoryInterface;

class CategoryRepository implements CategoryRepositoryInterface
{
    public function getAllCategories()
    {
        return Category::query()->latest()->get();
    }

    public function getCategories($perPage)
    {
        return Category::query()->where('is_active', true)->latest()->paginate($perPage);
    }

    public function create(Category $category)
    {
        $category->save();
        return $category->refresh();
    }

    public function show($id)
    {
        return Category::query()->find($id);
    }

    public function edit($id, $data)
    {
        $category = $this->show($id);
        $category->update($data);
        return $category->refresh();
    }

    public function delete($id)
    {
        $category = $this->show($id);
        return $category->delete();
    }

    public function search($filters)
    {
        return Category::query()
            ->where('name', 'like', '%' . $filters['key'] . '%')->get();
    }
}
