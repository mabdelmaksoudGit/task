<?php

namespace App\Repositories\Interfaces;

use App\Models\Course;


interface CourseRepositoryInterface
{
    public function getAllCourses();

    public function getCourses($perPage);

    public function create(Course $course);

    public function show($id);

    public function delete($id);
}
