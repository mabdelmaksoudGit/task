<?php

use App\Http\Controllers\Dashboard\CourseController;
use App\Http\Controllers\Dashboard\CategoryController;
use Illuminate\Support\Facades\Route;


//basic crud operation
Route::apiResource('courses', CourseController::class);
Route::apiResource('categories', CategoryController::class);
//search and filters
Route::get('search/courses', [CourseController::class, 'search']);
Route::get('search/categories', [CategoryController::class, 'search']);
// update activity for each courses and categories
Route::put('course/activity/{id}', [CourseController::class, 'updateCourseActivity']);
Route::put('category/activity/{id}', [CategoryController::class, 'updateCategoryActivity']);
//mobile apis
Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('active/courses', [App\Http\Controllers\Mobile\CourseController::class, 'activeCourses']);
    Route::get('active/categories', [App\Http\Controllers\Mobile\CategoryController::class, 'activeCategories']);
});
//login api for mobile to get bearer token
Route::post('tokens/create', [App\Http\Controllers\Mobile\Auth\LoginController::class, 'createToken']);

