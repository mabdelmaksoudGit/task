<?php

namespace Database\Seeders;


use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        User::query()->create([
            'name' => 'test user',
            'email' => 'test@email.com',
            'password' => Hash::make('123456'),
        ]);
    }
}
