<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Course;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        foreach (Category::all() as $item) {
            Course::query()->create([
                'name' => $faker->name,
                'description' => $faker->text,
                'rating' => $faker->randomNumber(1),
                'views' => $faker->randomNumber(1),
                'hours' => $faker->randomNumber(1),
                'category_id' => $item->id,
            ]);
            Course::query()->create([
                'name' => $faker->name,
                'description' => $faker->text,
                'rating' => $faker->randomNumber(1),
                'views' => $faker->randomNumber(1),
                'hours' => $faker->randomNumber(1),
                'category_id' => $item->id,
            ]);
            Course::query()->create([
                'name' => $faker->name,
                'description' => $faker->text,
                'rating' => $faker->randomNumber(1),
                'views' => $faker->randomNumber(1),
                'hours' => $faker->randomNumber(1),
                'category_id' => $item->id,
            ]);
        }
    }
}
